# b64

Utility that implements the Base64 Content-Transfer-Encoding standard described in RFC1113.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## B64.LSM

<table>
<tr><td>title</td><td>b64</td></tr>
<tr><td>version</td><td>0.06R (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-02-01</td></tr>
<tr><td>description</td><td>Base64 Content-Transfer-Encoding</td></tr>
<tr><td>keywords</td><td>b64,base64</td></tr>
<tr><td>author</td><td>Bob Trower</td></tr>
<tr><td>original&nbsp;site</td><td>http://base64.sourceforge.net</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[MIT license](LICENSE)</td></tr>
<tr><td>summary</td><td>Utility that implements the Base64 Content-Transfer-Encoding standard described in RFC1113.</td></tr>
</table>
